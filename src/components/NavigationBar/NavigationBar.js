import React from 'react'
import './NavigationBar.css'
import Logo from '../../assets/images/logo-white.svg'

class NavigationBar extends React.Component {
	render() {
		return (
			<nav className='navigation-bar global-side-padding'>
				<div className='logo-container'>
					<a href='/'>
						<img className='logo-image' src={Logo} />
					</a>
				</div>
			</nav>
		)
	}
}

export default NavigationBar
